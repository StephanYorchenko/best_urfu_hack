import React from 'react';
import Line from './line';
class DirList extends React.Component{
    render() {
        if (this.props.items) {
            var list_dir = this.props.items.map(function (elem){
                return (<Line key={elem.name} name={elem.name} size={elem.size} modified={elem.modified} path={elem.path} type={elem.type}></Line>);
            });
        }
        return (
            <div className="view">
                <header>
                    <Line name="Название" size="Размер" modified="Дата изм." type="none"></Line>
                </header>
                {list_dir}
            </div>
        )
    }
}

export default DirList;